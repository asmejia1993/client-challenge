import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { RestService } from 'src/app/service/rest.service';
import { Post } from './post.model';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  constructor(private rest: RestService) { 
    this.getPosts();
  }

  ngOnInit(): void {
    
  }

  selectedPost : number;
  posts: Post[] = [];


  getPosts(): void {
    this.rest.getPosts().subscribe((resp: any) => {
      this.posts = resp;
      console.log(this.posts);
    });
  }

  removePost(id: String) :void {
    this.rest.removePost(id).subscribe(
      (data) => {
        this.getPosts();
      }
    );
    
  }

  toggle(index: number) {
    
    this.posts.filter(
    (post, i) => i !== index && post.active)
    .forEach(post => post.active = !post.active);

  this.posts[index].active = !this.posts[index].active;
}

}
