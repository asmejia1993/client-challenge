export class Post {
    id: number;
    title: string;
    author: string;
    created_at: string;
    url: string;
    hide: boolean;
    time: string;
    active: boolean;
}