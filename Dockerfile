# stage 1
FROM node:14.15.3-alpine3.10 as build
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build --prod

# Stage 2
FROM nginx:1.19.6-alpine
COPY --from=build /usr/src/app/dist/client-challenge /usr/share/nginx/html 
#COPY nginx-custom.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx","-g","daemon off;"]


